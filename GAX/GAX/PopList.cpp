#include <vector>
#include <string>
#include <iostream>
#include "PopList.hpp"

PopList::PopList(std::vector<Widget> population, unsigned short popID, std::string popTitle) : population(population), popID(popID), popTitle(popTitle)
{

}
PopList::PopList()
{

}

std::vector<Widget> PopList::getPop() const
{
	return population;
}
void PopList::setPop(std::vector<Widget> p)
{
	population = p;
}
unsigned short PopList::getPopID()
{
	return popID;
}
void PopList::setPopID(unsigned short n)
{
	popID = n;
}
unsigned short PopList::getTotFit() const
{
	return totFit;
}
void PopList::setTotFit(unsigned short t)
{
	totFit = t;
}
void PopList::plonk(PopList p, Widget w)
{
	std::vector<Widget> population = p.getPop();
	population.push_back(w);
	p.setPop(population);
}
void PopList::print()
{
	std::cout << " Population " + std::to_string(getPopID()) << std::endl;
	std::vector<Widget> clip = getPop();
	for (Widget w : clip)
	{
		w.print();
	}
}
