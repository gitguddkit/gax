#pragma once
#include <string>
class Utility;

namespace Utx
{
	std::string toBin(int fit);
	unsigned short binToDec(std::string bin);
	unsigned short randFit();
	float randSelect();
	char coinToss();
};
