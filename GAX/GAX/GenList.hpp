#pragma once
#include <vector>
#include "Widget.hpp"

class GenList;

class GenList
{
public:
	GenList();
	GenList(std::vector<PopList> generations);
	std::vector<PopList> getGen() const;
	void setGen(std::vector<PopList>);
	void print(GenList generations);
};
//not sure if this will print oldest first or no, so might need to experiment