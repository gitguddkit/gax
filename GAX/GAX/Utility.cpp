#include <string>
#include <bitset>
#include "Utility.hpp"

namespace Utx
{
	std::string toBin(int fit)
	{
		std::string bin = std::bitset<5>(fit).to_string(); //to binary
		return bin;
	}

	unsigned short binToDec(std::string bin)
	{
		unsigned short dec = (unsigned short)std::bitset<5>(bin).to_ulong();
		return dec;
	}

	unsigned short randFit()
	{
		unsigned short max = 31;
		unsigned short min = 0;
		unsigned short random = rand() % ((max - min) + min);
		return random;
	}
	float randSelect()
	{
		float select = (float)rand() / RAND_MAX;
		return select;
	}
	char coinToss()
	{
		char bit;
		unsigned short coin = rand() % 10 + 1;
		if (coin % 2 == 0)
			bit = 0;
		else
			bit = 1;
		return bit;
	}
}