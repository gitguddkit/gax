#include <iostream>
#include <string>
#include <vector>
#include "Widget.hpp"
#include "PopList.hpp"
#include <algorithm>
#include <functional>
#include "GenList.hpp"
#include "Utility.hpp"
#include <ctime>
#include <random>

std::vector<Widget> initPop0();//get details from UI and create basic gen 0
std::vector<Widget> sortAndCull(std::vector<Widget> p); //sort pop by fitness, determine elites, cull non-potential-parents
//above - applies cscore and norm and pushes to a parent pool vector
//for each child, select 2 parents (random float vs norm of parents); push selected to new vector
//encode each parent (fit to dna) and either do crossover or locus randomizing
std::vector<Widget> cScoreAndNorm(std::vector<Widget> p);//compute cumulative fitness per Widget, get total Fitness, and add norms
std::vector<Widget> encodeDNA(std::vector<Widget> p);//convert fitness to binary and set
std::vector<Widget> makeBabies(std::vector<Widget> p);//select parents, crossover, mutate, add children to pop
//wash, rinse, and repeat gens times
//print first gen and final gen
void finish(GenList g);
unsigned short pop;//num in pop
unsigned short gens;//num of gens
unsigned short fit;//fitness 
unsigned short mOr, mor2;//manual OR random 
unsigned short counter;//num holder, 
unsigned short totFit;//total fitness of Pop N
unsigned short genNum; //counter - which gen we're working on
unsigned short elites; //how many in population move to next pop
unsigned short unlucky; // how many to cull
unsigned short popPool; //pop - unlucky
unsigned short babiesNeeded; //pop - elites
unsigned short parents;//babies * 2
bool valid = false;//bool switch for whiles
float norm;//normalization value cscore / totFit
std::vector<Widget> holder, holder2, holder3, newPop, parentPop;
PopList population;
std::vector<PopList> generations;
//GenList generations;
//Utility utils;
std::string whatevs = "whatevs";//DEBUGGING

int main()
{
	genNum = 0;
	srand(static_cast <unsigned> (time(0)));
	std::vector<Widget> widgets = initPop0();
	sortAndCull(widgets);
	makeBabies(widgets);
	return 0;
}
std::vector<Widget> initPop0()
{
	std::cout << "Welcome to the Genetic Algorithm eXperiment (GAX)\n\n" << std::endl;
	std::cout << "You will experiment with populations of widgets to extrude the fittest population \n" << std::endl;
	std::cout << "You will specify the number of widgets in each population, and the number of generations you wish to create \n" << std::endl;
	while (!valid)
	{
		std::cout << "Please enter the number of widgets in each population (5 - 15):" << std::endl;
		std::cin >> pop;
		if (pop < 5 || pop >15)
			std::cout << "You entered a number that is too low or too high. Please try again." << std::endl;
		else
			valid = true;
	}
	valid = false;
	while (!valid)
	{
		std::cout << "Please enter the number of generations you wish to generate (2-100):" << std::endl;
		std::cin >> gens;
		if (gens < 2 || gens > 100)
			std::cout << "You entered a number that is too low or too high. Please try again." << std::endl;
		else
			valid = true;
	}
	valid = false;
	while (!valid)
	{

		std::cout << "Do you wish to enter fitness values, or randomize all fitness values? Enter 1 for manual control OR 2 for random. " << std::endl;
		std::cin >> mOr;
		if (mOr < 1 || mOr >2)
			std::cout << "Enter 1 for manual or 2 for random! " << std::endl;
		else
			valid = true;
	}
	if (mOr == 1)
	{
		for (unsigned short j = 0; j < pop; j++)
		{
			valid = false;
			bool valid2 = false;
			while (!valid)
			{
				while (!valid2)
				{
					std::cout << "Manual or Random Fitness? 1 for manual, 2 for random" << std::endl;
					std::cin >> mor2;
					if (mor2 > 0 && mor2 < 3)
						valid2 = true;
				}
				if (mor2 == 1)
				{
					std::cout << "Manual or Random Fitness? 1 for manual, 2 for random";
					std::cout << "Please enter a fitness for population member" + std::to_string(j + 1) + " :" << std::endl;
					std::cin >> fit;
					if (fit < 0 || fit > 31)
						std::cout << "You entered a number that is too low or too high. Please try again." << std::endl;
					else
						valid = true;
				}
				else if (mor2 == 2)
				{
					fit = Utx::randFit();
					valid = true;
				}
			}
			Widget w((j + 1), fit);
			holder.push_back(w);


		}
	}
	else if (mOr == 2)
	{
		for (unsigned short j = 0; j < pop; j++)
		{
			unsigned int fit = Utx::randFit();
			Widget w((j + 1), fit);
			holder.push_back(w);
		}
	}
	//start debug
	std::cout << "Generation 0: " << std::endl;
	std::cout << "print1 - initialize pop /n /n" << std::endl;
	for (Widget w : holder)
		w.print();
	//end debug
	std::string popTitle = "population" + std::to_string(genNum);
	PopList population(holder, genNum, popTitle);
	std::vector<PopList> generations;
	generations.push_back(population);
	return holder;
}
std::vector<Widget> sortAndCull(std::vector<Widget> p) //sort pop by fitness, determine elites, cull non-elites.
{
	holder2 = holder;
	holder.clear();
	std::sort(holder2.begin(), holder2.end());
	counter = 0;
	unsigned int temp = 0;
	//for (unsigned short k = 0; k < holder2.size(); k++)
	for (unsigned short m = 0; m < holder2.size(); m++)
	{
		Widget p = holder2[m];
		counter += p.getFit();
		p.setCscore(counter);
		holder3.push_back(p);
	}
	totFit = counter;
	population.setTotFit(totFit);
	std::vector<PopList> generations;
	generations.push_back(population);
	counter = 0;
	holder2.clear();
	for (Widget w : holder3)
	{
		Widget proxy = w;
		proxy.setNorm(((float)w.getCscore()) / (float)totFit);
		std::string genes = Utx::toBin(proxy.getFit());
		proxy.setDNA(genes);
		holder2.push_back(proxy);
	}
	holder3.clear();
	elites = pop / 3;
	if (elites % 2 != 0)
		elites++;
	std::cout << "Elite Widgets: " + std::to_string(elites) << std::endl;//debug
	unlucky = pop / 4;
	std::cout << "Culled Widgets: " + std::to_string(unlucky) << std::endl;//debug
	popPool = pop - unlucky;
	std::cout << "Potential Parents: " + std::to_string(popPool) << std::endl;//debug
	babiesNeeded = pop - elites;
	std::cout << "Children Needed: "+ std::to_string(babiesNeeded) << std::endl;//debug
	parents = babiesNeeded * 2;
	std::cout << "Parents required: " + std::to_string(parents) << std::endl;//debug
	for (unsigned short q = 0; q < popPool; q++)
	{
		Widget temp = holder2.at(q);
		holder.push_back(temp);
	}
	holder2.clear();
	holder3.clear();
	//start debug
	std::cout << "Generation 0 parents: " << std::endl;
	for (Widget w : holder)
		w.print();
	std::cout << std::to_string(holder.size()) << std::endl;
	//end debug --so we have our group of parents from gen 0, moving on to selection
	float blah = Utx::randSelect();
	std::cout << blah << std::endl;
	return holder;
}
std::vector<Widget> makeBabies(std::vector<Widget> u)
{
	counter = 0;
	holder2.clear();
	holder2 = u;
	for (unsigned short b = 0; b < babiesNeeded; b++)
	{
		for (unsigned short d = 0; d < 2; d++)
		{
			float selector = Utx::randSelect();
			for (Widget w : holder)
			{
				float norm = w.getNorm();
				if (selector >= norm)
				{
					parentPop.push_back(w);
				}
				else
				{
				
				}

			}
		}
		for (unsigned short j = 0; j < elites; j++)
		{
			Widget u = parentPop.at(j);
			newPop.push_back(u);
		}
		for (unsigned short k = 0; k < babiesNeeded; (k + 2))
		{
			char locus;
			Widget a = parentPop.at(k);
			Widget b = parentPop.at(k + 1);
			std::string chromA = a.getDNA();
			char geneA[5] = { 0 };
			std::copy(chromA.begin(), chromA.end(), geneA);
			std::string chromB = b.getDNA();
			char geneB[5] = { 0 };
			std::copy(chromB.begin(), chromB.end(), geneB);
			char geneC[5] = { 0 };//child
			for (unsigned short l = 0; l < 5; l++)
			{
				if (geneA[l] = geneB[l])
					locus = geneA[l];
				else
					locus = Utx::coinToss();
				geneC[l] = locus;
			}
			std::string chromC = geneC;
			unsigned short childFit = Utx::binToDec(chromC);
			//DEBUG
			std::cout << "Yo, babie's fitness!!: " + std::to_string(childFit);
		}

		//for ()
		//rest of child stuff here
		//transfer elites to next pop
		//load next pop with children here
		//return vector with next pop
	}
	//start debug
	std::cout << "Parents: " << std::endl;
	for (Widget w : holder3)
		w.print();
	std::cin >> whatevs;
	return holder2;
	//end debug
}


/*totFit = counter;
counter = 0;
holder.clear();
for (Widget w: holder3)
{
	Widget zed = w;
	unsigned short n1 = zed.getCscore();
	float test = (float)n1 / (float)totFit;
	std::cout << test << std::endl;
	w.setNorm(test);
	holder.push_back(w);
}
//DEBUG
for (Widget w : holder)
{

	w.print();
}
std::cout << "Total Fitness :" + std::to_string(totFit) << std::endl;
//end debug print
//let's get elites, lets says 1/3rd pop 0
elites = pop / 3;
babiesNeeded = pop - elites;
parents = babiesNeeded * 2;
std::vector<Widget> survivors ;
for (unsigned short n = 0; n < holder.size(); n++)
{
	if (n <= elites-1)
	{
		Widget r = holder.at(n);
		survivors.push_back(r);
	}
}
//another debug
std::cout << "Survivor Pop. " << std::endl;
for (Widget w : survivors)
{
	w.print();
}
//end debug
std::cin >> whatevs;//TO PAUSE
population.setPop(holder);
population.setPopID(0);
population.setTotFit(totFit);
holder2.clear();
holder3.clear();
holder.clear();
//run ranking on population 0
//sort population 0
//select elites, move to pop1
//select remainder of parents, move to pop1
//select parents
//randomize crossover point per population
//randomize mutation on children
//add babies until pop 1 is complete
//repeat for n generations*/