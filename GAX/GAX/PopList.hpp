#pragma once
#include <vector>
#include "Widget.hpp"
#include <string>
class PopList;

class PopList
{
public:
	std::vector<Widget> population;
	unsigned short popID, totFit;
	std::string popTitle;
	PopList();
	PopList(std::vector<Widget> population, unsigned short popID, std::string popTitle);
	void print();
	std::vector<Widget> getPop() const;
	void setPop(std::vector<Widget> population);
	unsigned short getPopID();
	void setPopID(unsigned short);
	unsigned short getTotFit() const;
	void setTotFit(unsigned short);

	void plonk(PopList p, Widget w);
};

