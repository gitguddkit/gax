#pragma once
#include <string>

class Widget
{
	unsigned short wID, fit, cScore;
	float norm;
	std::string dna;
	public:
		Widget();
		Widget(unsigned short wID, unsigned short fit, unsigned short cScore, float norm, std::string dna);
		Widget(unsigned short wID, unsigned short fit);
		bool Widget::operator < (const Widget &other) const
		{
			return other.fit < fit;
		}
		void print();
		void setWID(unsigned short x);
		unsigned short getWID() const;
		void setFit(unsigned short y);
		unsigned short getFit() const;
		void setCscore(unsigned short z);
		unsigned short getCscore() const;
		float getNorm() const;
		void setNorm(float n);
		std::string getDNA() const;
		void setDNA(std::string s);
};
