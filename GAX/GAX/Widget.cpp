#include "Widget.hpp"
#include <iostream>
#include <string>
#include <iomanip>

Widget::Widget(unsigned short wID, unsigned short fit, unsigned short cScore, float norm, std::string dna) : wID(wID), fit(fit), cScore(cScore), norm(norm), dna(dna)
{

}
Widget::Widget(unsigned short wID, unsigned short fit) : wID(wID), fit(fit)
{
	cScore = 0;
	norm = 0.f;
}
Widget::Widget()
{

}
void Widget::setWID(unsigned short x)
{
	wID = x;
}
unsigned short Widget::getWID() const
{
	return wID;
}
void Widget::setFit(unsigned short y)
{
	fit = y;
}
unsigned short Widget::getFit() const
{
	return fit;
}
void Widget::setCscore(unsigned short z)
{
	cScore = z;
}
unsigned short Widget::getCscore() const
{
	return cScore;
}
float Widget::getNorm() const
{
	return norm;
}
void Widget::setNorm(float n)
{
	norm = n;
}
std::string Widget::getDNA() const
{
	return dna;
}
void Widget::setDNA(std::string s)
{
	dna = s;
}
void Widget::print()
{
	std::cout << "Widget ID: " + std::to_string(getWID()) << std::endl;
	std::cout << "Widget Fitness: " + std::to_string(getFit()) << std::endl;
	std::cout << "Widget Cumulative Score: " + std::to_string(getCscore()) << std::endl;
	std::cout << "Widget Normalization Value: " + std::to_string(getNorm()) << std::endl;
	std::cout << "Widget DNA: " + dna << std::endl;
}